package paqCocinas;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DlgConsultarCocina extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JLabel lblNewLabel;
	private JLabel lblModelo;
	private JLabel lblAnchocm;
	private JLabel lblAltocm;
	private JLabel lblFondocm;
	private JLabel lblQuemadores;
	private JComboBox cboModelos;
	private JButton btnCerrar;
	private JTextField txtPrecio;
	private JTextField txtAncho;
	private JTextField txtAlto;
	private JTextField txtFondo;
	private JTextField txtQuemadores;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgConsultarCocina dialog = new DlgConsultarCocina();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public DlgConsultarCocina() {
		setTitle("Consultar Cocina");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("Modelo");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(40, 44, 46, 14);
		getContentPane().add(lblNewLabel);
		
		lblModelo = new JLabel("Precio (S/)");
		lblModelo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblModelo.setBounds(40, 72, 59, 14);
		getContentPane().add(lblModelo);
		
		lblAnchocm = new JLabel("Ancho (cm)");
		lblAnchocm.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAnchocm.setBounds(40, 97, 73, 14);
		getContentPane().add(lblAnchocm);
		
		lblAltocm = new JLabel("Alto (cm)");
		lblAltocm.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAltocm.setBounds(40, 122, 59, 14);
		getContentPane().add(lblAltocm);
		
		lblFondocm = new JLabel("Fondo (cm)");
		lblFondocm.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFondocm.setBounds(40, 147, 73, 14);
		getContentPane().add(lblFondocm);
		
		lblQuemadores = new JLabel("Quemadores");
		lblQuemadores.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblQuemadores.setBounds(40, 172, 73, 14);
		getContentPane().add(lblQuemadores);
		
		cboModelos = new JComboBox();
		cboModelos.addActionListener(this);
		cboModelos.setModel(new DefaultComboBoxModel(new String[] {"Mabe EMP6120PG0", "Indurama Parma", "Sole COSOL027", "Coldex CX602", "Reco Dakota"}));
		cboModelos.setBounds(141, 44, 167, 22);
		getContentPane().add(cboModelos);
		
		btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(this);
		btnCerrar.setBounds(335, 41, 89, 23);
		getContentPane().add(btnCerrar);
		
		txtPrecio = new JTextField();
		txtPrecio.setEditable(false);
		txtPrecio.setBounds(141, 70, 167, 20);
		getContentPane().add(txtPrecio);
		txtPrecio.setColumns(10);
		
		txtAncho = new JTextField();
		txtAncho.setEditable(false);
		txtAncho.setColumns(10);
		txtAncho.setBounds(141, 95, 167, 20);
		getContentPane().add(txtAncho);
		
		txtAlto = new JTextField();
		txtAlto.setEditable(false);
		txtAlto.setColumns(10);
		txtAlto.setBounds(141, 120, 167, 20);
		getContentPane().add(txtAlto);
		
		txtFondo = new JTextField();
		txtFondo.setEditable(false);
		txtFondo.setColumns(10);
		txtFondo.setBounds(141, 145, 167, 20);
		getContentPane().add(txtFondo);
		
		txtQuemadores = new JTextField();
		txtQuemadores.setEditable(false);
		txtQuemadores.setColumns(10);
		txtQuemadores.setBounds(141, 170, 167, 20);
		getContentPane().add(txtQuemadores);

	}
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cboModelos) {
			cboModelosActionPerformed(e);
		}
		if (e.getSource() == btnCerrar) {
			btnCerrarActionPerformed(e);
		}
	}
	protected void btnCerrarActionPerformed(ActionEvent e) {
		dispose();
	}
	protected void cboModelosActionPerformed(ActionEvent e) {
		//obtener el �ndice del modelo seleccionado
		int indice = cboModelos.getSelectedIndex();
		
		switch (indice) {
		
		case 0:
			txtPrecio.setText(FrmMenuPrincipal.precio0+"");
			txtAncho.setText(FrmMenuPrincipal.ancho0+"");
			txtAlto.setText(FrmMenuPrincipal.alto0+"");
			txtFondo.setText(FrmMenuPrincipal.fondo0+"");
			txtQuemadores.setText(FrmMenuPrincipal.quemadores0+"");
			break;
		case 1:
			txtPrecio.setText(FrmMenuPrincipal.precio1+"");
			txtAncho.setText(FrmMenuPrincipal.ancho1+"");
			txtAlto.setText(FrmMenuPrincipal.alto1+"");
			txtFondo.setText(FrmMenuPrincipal.fondo1+"");
			txtQuemadores.setText(FrmMenuPrincipal.quemadores1+"");
			break;
		case 2:
			txtPrecio.setText(FrmMenuPrincipal.precio2+"");
			txtAncho.setText(FrmMenuPrincipal.ancho2+"");
			txtAlto.setText(FrmMenuPrincipal.alto2+"");
			txtFondo.setText(FrmMenuPrincipal.fondo2+"");
			txtQuemadores.setText(FrmMenuPrincipal.quemadores2+"");
			break;
		case 3:
			txtPrecio.setText(FrmMenuPrincipal.precio3+"");
			txtAncho.setText(FrmMenuPrincipal.ancho3+"");
			txtAlto.setText(FrmMenuPrincipal.alto3+"");
			txtFondo.setText(FrmMenuPrincipal.fondo3+"");
			txtQuemadores.setText(FrmMenuPrincipal.quemadores3+"");
			break;
		case 4:
			txtPrecio.setText(FrmMenuPrincipal.precio4+"");
			txtAncho.setText(FrmMenuPrincipal.ancho4+"");
			txtAlto.setText(FrmMenuPrincipal.alto4+"");
			txtFondo.setText(FrmMenuPrincipal.fondo4+"");
			txtQuemadores.setText(FrmMenuPrincipal.quemadores4+"");
			break;
		}		
		
	}
}
