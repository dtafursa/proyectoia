package paqCocinas;


import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmMenuPrincipal extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
		
	// Datos m�nimos de la primera cocina
	public static String modelo0 = "Mabe EMP6120PG0";
	public static double precio0 = 949.0;
	public static double fondo0 = 58.6;
	public static double ancho0 = 60.0;
	public static double alto0 = 91.0;
	public static int quemadores0 = 4;
	// Datos m�nimos de la segunda cocina
	public static String modelo1 = "Indurama Parma";
	public static double precio1 = 1089.0;
	public static double ancho1 = 80.0;
	public static double alto1 = 94.0;
	public static double fondo1 = 67.5;
	public static int quemadores1 = 6;
	// Datos m�nimos de la tercera cocina
	public static String modelo2 = "Sole COSOL027";
	public static double precio2 = 850.0;
	public static double ancho2 = 60.0;
	public static double alto2 = 90.0;
	public static double fondo2 = 50.0;
	public static int quemadores2 = 4;
	// Datos m�nimos de la cuarta cocina
	public static String modelo3 = "Coldex CX602";
	public static double precio3 = 629.0;
	public static double ancho3 = 61.6;
	public static double alto3 = 95.0;
	public static double fondo3 = 51.5;
	public static int quemadores3 = 5;
	// Datos m�nimos de la quinta cocina
	public static String modelo4 = "Reco Dakota";
	public static double precio4 = 849.0;
	public static double ancho4 = 75.4;
	public static double alto4 = 94.5;
	public static double fondo4 = 66.0;
	public static int quemadores4 = 5;
	// Porcentajes de descuento
	public static double porcentaje1 = 7.5;
	public static double porcentaje2 = 10.0;
	public static double porcentaje3 = 12.5;
	public static double porcentaje4 = 15.0;
	// Obsequios
	public static String obsequio1 = "Cafetera";
	public static String obsequio2 = "Licuadora";
	public static String obsequio3 = "Extractor";
	// Cantidad �ptima de unidades vendidas
	public static int cantidadOptima = 30;
	// Cuota diaria
	public static double cuotaDiaria = 75000;
		
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnArchivo;
	private JMenu mnMantenimiento;
	private JMenu mnAyuda;
	private JMenuItem mntmSalir;
	private JMenuItem mntmConsultarCocina;
	private JMenuItem mntmListarCocinas;
	private JMenuItem mntmAcercaDeTiendas;
	private JMenuItem mntmModificarCocina;
	private JMenu mnVentas;
	private JMenu mnConfiguracion;
	private JMenuItem mntmVender;
	private JMenuItem mntmGenerarReportes;
	private JMenuItem mntmConfigDesct;
	private JMenuItem mntmConfigObsq;
	private JMenuItem mntmConfigCant;
	private JMenuItem mntmConfigCuota;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmMenuPrincipal frame = new FrmMenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmMenuPrincipal() {
		setTitle("Tienda 1.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(this);
		mnArchivo.add(mntmSalir);
		
		mnMantenimiento = new JMenu("Mantenimiento");
		menuBar.add(mnMantenimiento);
		
		mntmConsultarCocina = new JMenuItem("Consultar Cocinas");
		mntmConsultarCocina.addActionListener(this);
		mnMantenimiento.add(mntmConsultarCocina);
		
		mntmModificarCocina = new JMenuItem("Modificar Cocina");
		mntmModificarCocina.addActionListener(this);
		mnMantenimiento.add(mntmModificarCocina);
		
		mntmListarCocinas = new JMenuItem("Listar Cocinas");
		mntmListarCocinas.addActionListener(this);
		mnMantenimiento.add(mntmListarCocinas);
		
		mnVentas = new JMenu("Ventas");
		menuBar.add(mnVentas);
		
		mntmVender = new JMenuItem("Vender");
		mntmVender.addActionListener(this);
		mnVentas.add(mntmVender);
		
		mntmGenerarReportes = new JMenuItem("Generar Reportes");
		mnVentas.add(mntmGenerarReportes);
		
		mnConfiguracion = new JMenu("Configuraci\u00F3n");
		menuBar.add(mnConfiguracion);
		
		mntmConfigDesct = new JMenuItem("Configurar descuentos");
		mnConfiguracion.add(mntmConfigDesct);
		
		mntmConfigObsq = new JMenuItem("Configurar obsequios");
		mnConfiguracion.add(mntmConfigObsq);
		
		mntmConfigCant = new JMenuItem("Configurar cantidad \u00F3ptima");
		mnConfiguracion.add(mntmConfigCant);
		
		mntmConfigCuota = new JMenuItem("Configurar cuota diaria");
		mnConfiguracion.add(mntmConfigCuota);
		
		mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		mntmAcercaDeTiendas = new JMenuItem("Acerca de Tiendas");
		mntmAcercaDeTiendas.addActionListener(this);
		mnAyuda.add(mntmAcercaDeTiendas);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mntmVender) {
			mntmVenderActionPerformed(e);
		}
		if (e.getSource() == mntmModificarCocina) {
			mntmModificarCocinaActionPerformed(e);
		}
		if (e.getSource() == mntmConsultarCocina) {
			mntmConsultarCocinaActionPerformed(e);
		}
		if (e.getSource() == mntmListarCocinas) {
			mntmListarCocinasActionPerformed(e);
		}
		if (e.getSource() == mntmSalir) {
			mntmSalirActionPerformed(e);
		}
		if (e.getSource() == mntmAcercaDeTiendas) {
			mntmAcercaDeTiendasActionPerformed(e);
		}
	}
	protected void mntmAcercaDeTiendasActionPerformed(ActionEvent e) {
		//llamar a Jdialog DlgAcercaDe
		//instanciar al Jdialaog
		DlgAcercaDe dialogo = new DlgAcercaDe();
		//visualizar al Jdialog
		dialogo.setVisible(true);
		
	}
	protected void mntmSalirActionPerformed(ActionEvent e) {
		//cerrar de la aplicacion
		System.exit(0);
	}
	protected void mntmListarCocinasActionPerformed(ActionEvent e) {
		ListarCocinas dialogo = new ListarCocinas();
		//visualizar al Jdialog
		dialogo.setVisible(true);
	}
	protected void mntmConsultarCocinaActionPerformed(ActionEvent e) {
		DlgConsultarCocina dlg = new DlgConsultarCocina();
		dlg.setVisible(true);
	}
	protected void mntmModificarCocinaActionPerformed(ActionEvent e) {
		DlgModificarCocina dlg = new DlgModificarCocina();
		dlg.setVisible(true);
	}
	protected void mntmVenderActionPerformed(ActionEvent e) {
		DlgVender dlg = new DlgVender();
		dlg.setVisible(true);
	}
}
