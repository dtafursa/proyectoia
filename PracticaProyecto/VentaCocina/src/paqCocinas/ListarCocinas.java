package paqCocinas;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ListarCocinas extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane;
	private JTextArea txtS;
	private JButton btnCerrar;
	private JButton btnListar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListarCocinas dialog = new ListarCocinas();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public ListarCocinas() {
		setTitle("Listado de Cocinas");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 204);
		getContentPane().add(scrollPane);
		
		txtS = new JTextArea();
		scrollPane.setViewportView(txtS);
		
		btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(this);
		btnCerrar.setBounds(97, 226, 89, 23);
		getContentPane().add(btnCerrar);
		
		btnListar = new JButton("Listar");
		btnListar.addActionListener(this);
		btnListar.setBounds(228, 226, 89, 23);
		getContentPane().add(btnListar);

	}
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnListar) {
			btnListarActionPerformed(e);
		}
		if (e.getSource() == btnCerrar) {
			btnCerrarActionPerformed(e);
		}
	}
	protected void btnCerrarActionPerformed(ActionEvent e) {
		dispose();
	}
	
	protected void btnListarActionPerformed(ActionEvent e) {
		txtS.setText("LISTADO DE COCINAS\n\n");
		txtS.append("Modelo	: " + FrmMenuPrincipal.modelo0 + "\n");
		txtS.append("Precio	: S/ " + FrmMenuPrincipal.precio0 + "\n");
		txtS.append("Profundidad	: " + FrmMenuPrincipal.fondo0 + " cm\n");
		txtS.append("Ancho	: " + FrmMenuPrincipal.ancho0 + " cm\n");
		txtS.append("Alto	: " + FrmMenuPrincipal.alto0 + " cm\n");
		txtS.append("Quemadores	: " + FrmMenuPrincipal.quemadores0 + "\n\n");
		
		txtS.append("Modelo	: " + FrmMenuPrincipal.modelo1 + "\n");
		txtS.append("Precio	: S/ " + FrmMenuPrincipal.precio1 + "\n");
		txtS.append("Profundidad	: " + FrmMenuPrincipal.fondo1 + " cm\n");
		txtS.append("Ancho	: " + FrmMenuPrincipal.ancho1 + " cm\n");
		txtS.append("Alto	: " + FrmMenuPrincipal.alto1 + " cm\n");
		txtS.append("Quemadores	: " + FrmMenuPrincipal.quemadores1 + "\n\n");
		
		txtS.append("Modelo	: " + FrmMenuPrincipal.modelo2 + "\n");
		txtS.append("Precio	: S/ " + FrmMenuPrincipal.precio2 + "\n");
		txtS.append("Profundidad	: " + FrmMenuPrincipal.fondo2 + " cm\n");
		txtS.append("Ancho	: " + FrmMenuPrincipal.ancho2 + " cm\n");
		txtS.append("Alto	: " + FrmMenuPrincipal.alto2 + " cm\n");
		txtS.append("Quemadores	: " + FrmMenuPrincipal.quemadores2 + "\n\n");
		
		txtS.append("Modelo	: " + FrmMenuPrincipal.modelo3 + "\n");
		txtS.append("Precio	: S/ " + FrmMenuPrincipal.precio3 + "\n");
		txtS.append("Profundidad	: " + FrmMenuPrincipal.fondo3 + " cm\n");
		txtS.append("Ancho	: " + FrmMenuPrincipal.ancho3 + " cm\n");
		txtS.append("Alto	: " + FrmMenuPrincipal.alto3 + " cm\n");
		txtS.append("Quemadores	: " + FrmMenuPrincipal.quemadores3 + "\n\n");
		
		txtS.append("Modelo	: " + FrmMenuPrincipal.modelo4 + "\n");
		txtS.append("Precio	: S/ " + FrmMenuPrincipal.precio4 + "\n");
		txtS.append("Profundidad	: " + FrmMenuPrincipal.fondo4 + " cm\n");
		txtS.append("Ancho	: " + FrmMenuPrincipal.ancho4 + " cm\n");
		txtS.append("Alto	: " + FrmMenuPrincipal.alto4 + " cm\n");
		txtS.append("Quemadores	: " + FrmMenuPrincipal.quemadores4 + "\n\n");
	}
}
